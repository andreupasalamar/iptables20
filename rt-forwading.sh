#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

#echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 192.168.1.50 -j ACCEPT
iptables -A OUTPUT -d 192.168.1.50 -j ACCEPT

# Fa nat de la xarxa interna: 172.19.0.0/24
iptables -t nat -A POSTROUTING -s 172.19.0.0/24 -o enp5s0 -j MASQUERADE

# Permetre a A1 fer de servidor daytime
iptables -A FORWARD -p tcp --dport 13 -d 172.19.0.2 -j ACCEPT
iptables -A FORWARD -p tcp --sport 13 -s 172.19.0.2 -d 0.0.0.0/0 -m \
	state --state RELATED,ESTABLISHED -j ACCEPT

# Volem permetre des de l'exterior accedir al servei 13 de A1
# Port forwarding 5013 --> 13 A1
iptables -t nat -A PREROUTING -p tcp --dport 5013 -j DNAT --to 172.19.0.2:13

# El forward ja està accept per defecte
#iptables -A FORWARD -p tcp --dport 13 -j REJECT
#iptables -A FORWARD -p tcp --sport 13 -j REJECT



















