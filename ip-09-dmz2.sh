#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

#echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 192.168.1.50 -j ACCEPT
iptables -A OUTPUT -d 192.168.1.50 -j ACCEPT

# Fer NAT de les xarxes internes
iptables -t nat -A POSTROUTING -s 172.21.0.0/24 -o eno1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/24 -o eno1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.10.10.0/24 -o eno1 -j MASQUERADE

# XarxaA pot accedir al servei web de DMZ
iptables -A FORWARD -s 172.21.0.0/24 -d 10.10.10.0/24 -p tcp --dport 80 -j ACCEPT
iptables -A FORWARD -s 10.10.10.0/24 -d 172.21.0.0/24 -p tcp --sport 80 -m state \
	--state ESTABLISHED,RELATED -j ACCEPT

# Es permet fer DNS a tothom
iptables -A FORWARD -o eno1 -p udp --dport 53 -j ACCEPT
iptables -A FORWARD -i eno1 -p udp --sport 53 -j ACCEPT
iptables -A FORWARD -o eno1 -p tcp --dport 53 -j ACCEPT
iptables -A FORWARD -i eno1 -p tcp --sport 53 -m state \
	--state ESTABLISHED,RELATED -j ACCEPT

# Xarxa B pot navegar per internet
iptables -A FORWARD -s 172.22.0.0/24 -o eno1 -p tcp  --dport 80 -j ACCEPT
iptables -A FORWARD -i eno1 -p tcp --sport 80 -d 172.22.0.0/24 -m state \
	--state ESTABLISHED,RELATED -j ACCEPT

# de la A es pot accedir a la B
iptables -A FORWARD -s 172.21.0.0/24 -d 172.22.0.0/24 -j ACCEPT
iptables -A FORWARD -s 172.22.0.0/24 -d 172.21.0.0/24 -j ACCEPT

# Port forward 3001 router 13 dmz
iptables -A FORWARD -i eno1 -d 10.0.0.2 -p tcp --dport 13 -j ACCEPT
iptables -A FORWARD -s 10.0.0.2 -p tcp --sport 13 -o eno1 -m state \
	--state ESTABLISHED,RELATED -j ACCEPT
iptables -t nat -A PREROUTING -p tcp --dport 3001 -j DNAT --to 10.10.10.2:13




