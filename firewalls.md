# Firewalls

*Andreu Pasalamar Carbó*

Tot trafic que s'origina al meu host i va cap un lloc es output.

Qualsevol trafic destinat al ordinador on s'apliquen les iptables es trafic input.

Quan faig un ping a localhost es output i input al mateix temps.

