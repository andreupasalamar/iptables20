#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

#echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 192.168.1.50 -j ACCEPT
iptables -A OUTPUT -d 192.168.1.50 -j ACCEPT

# NAT xarxes internes
iptables -t nat -A POSTROUTING -s 172.18.0.0/24 -o eno1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.21.0.0/24 -o eno1 -j MASQUERADE

# no permetre xarxaA accedir a host de xarxaB
# iptables -A FORWARD -s 172.18.0.0/24 -d 172.21.0.0/24 -j REJECT

# no permetre de xarxaA accedir al host B1
# iptables -A FORWARD -s 172.18.0.0/24 -d 172.21.0.2 -j REJECT

# A1 no pot conectar per res a B1
# iptables -A FORWARD -s 172.18.0.2 -d 172.21.0.2 -j REJECT

# xarxaA te vetat accedir al port 13 de on sigui
# iptables -A FORWARD -i br-6da089fe747d -p tcp --dport 13 -j REJECT

# xarxaA no pot accedir al port 2013 de la xarxaB
# iptables -A FORWARD -i br-6da089fe747d -p tcp -d 172.21.0.0/24 --dport 2013 -j REJECT

# xarxaA pot navegar per internet pero no pot fer res mes a l'exterior.
iptables -A FORWARD -i br-6da089fe747d -p tcp -o eno1 --dport 80 -j ACCEPT
iptables -A FORWARD -i eno1 -d 172.18.0.0/24 -p tcp --sport 80 -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -i br-6da089fe747d -o eno1 -j DROP
iptables -A FORWARD -i eno1 -d 172.18.0.0/24 -j DROP



