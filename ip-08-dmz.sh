#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

#echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.243.216 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.216 -j ACCEPT

# Fer nat per les xarxes 172.21.0.0/24 172.22.0.0/24 192.168.100.0/24
iptables -t nat -A POSTROUTING -s 172.21.0.0/24 -o enp5s0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/24 -o enp5s0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -o enp5s0 -j MASQUERADE

# NetA només pot accedir al daytime i ssh del router
#iptables -A INPUT -s 172.21.0.0/24 -p tcp --dport 22 -j ACCEPT
#iptables -A INPUT -s 172.21.0.0/24 -p tcp --dport 13 -j ACCEPT
#iptables -A INPUT -s 172.21.0.0/24 -j DROP

# NetA només es pot accedir a l'exterior als serveis web,ssh i daytime
#iptables -A FORWARD -s 172.21.0.0/24 -p tcp --dport 80 -o enp5s0 -j ACCEPT
#iptables -A FORWARD -i enp5s0 -p tcp --sport 80 -d 172.21.0.0/24 -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/24 -p tcp --dport 22 -o enp5s0 -j ACCEPT
#iptables -A FORWARD -i enp5s0 -p tcp --sport 22 -d 172.21.0.0/24 -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/24 -p tcp --dport 13 -o enp5s0 -j ACCEPT
#iptables -A FORWARD -i enp5s0 -p tcp --sport 13 -d 172.21.0.0/24 -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/24 -o enp5s0 -j REJECT
#iptables -A FORWARD -d 172.21.0.0/24 -i enp5s0 -j REJECT

# NetA només pot accedir als serveis que ofereix la DMZ
#iptables -A FORWARD -s 172.21.0.0/24 -d 192.168.100.0/24 -j ACCEPT
#iptables -A FORWARD -d 172.21.0.0/24 -s 192.168.100.0/24 -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/24 -j DROP
#iptables -A FORWARD -d 172.21.0.0/24 -j DROP

# NetA només pot accedir als serveis web de la DMZ
#iptables -A FORWARD -s 172.21.0.0/24 -d 192.168.100.0/24 -p tcp --dport 80 -j ACCEPT
#iptables -A FORWARD -d 172.21.0.0/24 -s 192.168.100.0/24 -p tcp --sport 80 -m state --state RELATED,ESTABLISHED -j ACCEPT
#iptables -A FORWARD -s 172.21.0.0/24 -d 192.168.100.0/24 -j REJECT
#iptables -A FORWARD -d 172.21.0.0/24 -s 192.168.100.0/24 -j REJECT

# Redirigir els ports des de l'exterior te accès a 3001->A1:80 3002->A2:13, 3003->B1:80,3004->B2:13 
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 3001 -j DNAT --to 172.21.0.2:80
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 3002 -j DNAT --to 172.21.0.3:13
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 3003 -j DNAT --to 172.22.0.2:80
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 3004 -j DNAT --to 172.21.0.3:13

# Redirigir els ports des de l'exterior te accès a ssh desde 4001-4004
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 4001 -j DNAT --to 172.21.0.2:22
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 4002 -j DNAT --to 172.21.0.3:22
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 4003 -j DNAT --to 172.22.0.2:22
#iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 4004 -j DNAT --to 172.22.0.3:22

# El port 4000 serveix per accedir al port ssh del firewall si la ip origen es el i27
#iptables -t nat -A PREROUTING -s 10.200.243.227 -p tcp --dport 4000 -j DNAT --to 10.200.243.216:22

# netb acces a tot arreu menys neta
#iptables -A FORWARD -s 172.22.0.0/24 -d 172.21.0.0/24 -j REJECT
#iptables -A FORWARD -s 172.22.0.0/24 -j ACCEPT
























